package com.vaiosec.peter.radonk;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.util.Log;
import android.content.Intent;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class OARRegionSelectorActivity extends AppCompatActivity {

    private RadioGroup sex_radioGroup;
    private RadioButton male_radioButton;
    private RadioButton female_radioButton;

    private ImageButton head_imageButton;
    private ImageButton ent_imageButton;
    private ImageButton thorax_imageButton;
    private ImageButton abdomen_imageButton;

    private boolean isMale;
    private static final String TAG = "OARRegionSel";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oar_region_selection);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        isMale = true;
        addListenerOnSexChange();
        addListenerHeadSelected();
        addListenerENTSelected();
        addListenerThoraxSelected();
        addListenerAbdomenSelected();
    }

    public void addListenerOnSexChange() {
        sex_radioGroup = (RadioGroup) findViewById(R.id.sex_radioGroup);
        male_radioButton = (RadioButton) findViewById(R.id.male_radioButton);
        female_radioButton = (RadioButton) findViewById(R.id.female_radioButton);
        sex_radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.male_radioButton:
                        isMale = true;
                        Log.v(TAG, "male sex selected");
                        break;
                    case R.id.female_radioButton:
                        isMale = false;
                        Log.v(TAG, "female sex selected");
                        break;
                    default:
                        Log.v(TAG, "sex selection error");
                }
            }
        });
    }

    public void addListenerHeadSelected() {
        head_imageButton = (ImageButton) findViewById(R.id.head_imageButton);
        head_imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "head selected");
                startOARActivity("head");
            }
        });
    }

    public void addListenerENTSelected() {
        ent_imageButton = (ImageButton) findViewById(R.id.ent_imageButton);
        ent_imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "ENT selected");
                startOARActivity("ent");
            }
        });
    }

    public void addListenerThoraxSelected() {
        thorax_imageButton = (ImageButton) findViewById(R.id.thorax_imageButton);
        thorax_imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "thorax selected");
                startOARActivity("thorax");
            }
        });
    }

    public void addListenerAbdomenSelected() {
        abdomen_imageButton = (ImageButton) findViewById(R.id.abdomen_imageButton);
        abdomen_imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "abdomen selected");
                startOARActivity("abdomen");
            }
        });

    }
    private void startOARActivity(String region) {
        Intent intent = new Intent(this,OAR_Activity.class);
        intent.putExtra("region",region);
        intent.putExtra("sex",isMale);
        startActivity(intent);
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("RaDonk Welcome")
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

}

