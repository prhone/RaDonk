package com.vaiosec.peter.radonk;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import android.content.Context;

/**
 * Created by peter on 07/12/16.
 */

public class StrahlDatabaseHelper extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "strahl_params.db";
    private static final int DATABASE_VERSION = 11;

    public StrahlDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        setForcedUpgrade();
    }
}
