package com.vaiosec.peter.radonk;

/**
 * Created by peter on 25.12.16.
 */

public class HmTableClass {
    private int _fxPerDay;
    private int rows;
    private int cols; //7 columns, including dailyfx (col 0) and repair_halftime (col 1) columns, 10 rows
    private Double[][] HmValueMatrix;
    private Double[] halftimeHmPair;
    private Double[] badPair;
    public HmTableClass(int fxPerDay) {
        if(fxPerDay==3) {
            rows=10;
            cols=7;
            _fxPerDay=3;
        }
        if(fxPerDay==2) {
            rows=10;
            cols=8;
            _fxPerDay=2;
        }
        HmValueMatrix = new Double[rows][cols];
        halftimeHmPair = new Double[2];
        badPair = new Double[]{-1.0d,-1.0d};
        _fxPerDay=fxPerDay;
    }
    public void addTableEntry(int x, int y, double value) {
        if(x<rows && y<cols)
            HmValueMatrix[x][y]=value;
    }
    public Double getHmTableValue(int x, int y) {
        if(x<rows&&y<cols)
            return HmValueMatrix[x][y];
        else
            return -1.0d;
    }
    public int getFxPerDay() {
        return _fxPerDay;
    }
    public Double[] isExactMatch(double halftime, int hoursBetweenFractions) {
        for(int i=0;i<rows;i++) {
            if(HmValueMatrix[i][1]==halftime) {
                halftimeHmPair[0]=HmValueMatrix[i][1];
                switch (hoursBetweenFractions) {
                    case 3:
                        halftimeHmPair[1]=HmValueMatrix[i][2];
                        break;
                    case 4:
                        halftimeHmPair[1]=HmValueMatrix[i][3];
                        break;
                    case 5:
                        halftimeHmPair[1]=HmValueMatrix[i][4];
                        break;
                    case 6:
                        halftimeHmPair[1]=HmValueMatrix[i][5];
                        break;
                    case 8:
                        halftimeHmPair[1]=HmValueMatrix[i][6];
                        break;
                    case 10:
                        if(_fxPerDay==2) {
                            halftimeHmPair[1]=HmValueMatrix[i][7];
                        }
                        break;
                }
                return halftimeHmPair;
            }
        }
        return badPair;
    }
    public Double[] getLowerRepairHalftimeAndHm(double halftime, int hoursBetweenFractions, int numFxPerDay) {
        for(int i=1;i<rows;i++) {
            if(HmValueMatrix[i][1]>halftime) {
                halftimeHmPair[0]=HmValueMatrix[i-1][1];
                switch (hoursBetweenFractions) {
                    case 3:
                        halftimeHmPair[1]=HmValueMatrix[i-1][2];
                        break;
                    case 4:
                        halftimeHmPair[1]=HmValueMatrix[i-1][3];
                        break;
                    case 5:
                        halftimeHmPair[1]=HmValueMatrix[i-1][4];
                        break;
                    case 6:
                        halftimeHmPair[1]=HmValueMatrix[i-1][5];
                        break;
                    case 8:
                        halftimeHmPair[1]=HmValueMatrix[i-1][6];
                        break;
                    case 10:
                        if(numFxPerDay==2) {
                            halftimeHmPair[1]=HmValueMatrix[i-1][7];
                        }
                        break;
                }
                return halftimeHmPair;
            }
        }
        return badPair;
    }
    public Double[] getUpperRepairHalftimeAndHm(double halftime, int hoursBetweenFractions, int numFxPerDay) {
        for(int i=0;i<rows;i++) {
            if(HmValueMatrix[i][1]>halftime) {
                halftimeHmPair[0]=HmValueMatrix[i][1];
                switch (hoursBetweenFractions) {
                    case 3:
                        halftimeHmPair[1]=HmValueMatrix[i][2];
                        break;
                    case 4:
                        halftimeHmPair[1]=HmValueMatrix[i][3];
                        break;
                    case 5:
                        halftimeHmPair[1]=HmValueMatrix[i][4];
                        break;
                    case 6:
                        halftimeHmPair[1]=HmValueMatrix[i][5];
                        break;
                    case 8:
                        halftimeHmPair[1]=HmValueMatrix[i][6];
                        break;
                    case 10:
                        if(numFxPerDay==2) {
                            halftimeHmPair[1]=HmValueMatrix[i][7];
                        }
                        break;
                }
                return halftimeHmPair;
            }
        }
        return badPair;
    }
}
