package com.vaiosec.peter.radonk;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;


/**
 * Created by peter on 17.12.16.
 */

public class WelcomeActivity extends AppCompatActivity {
    private Button OARWelcomeButton;
    private Button doseLimitExplorerWelcomeButton;
    private Button tumorWelomeButton;
    private Button hyperfxWelcomButton;
    private Button rescheduleWelcomeButton;
    private Button LQCalcWelcomeButton;
    private Button extrasWelcomeButton;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        addListenerOnOARButtonClick();
        addListenerOnDoseLimitExplorerButtonClick();
        addListenerOnTumorButtonClick();
        addListenerOnHyperfxButtonClick();
        addListenerOnRescheduleButtonClick();
        addListenerOnLQCalcButtonClick();
        addListenerOnExtrasButtonClick();
    }

    public void addListenerOnOARButtonClick() {
        OARWelcomeButton = (Button) findViewById(R.id.oar_welcomebutton);
        OARWelcomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOARRegionSelectorActivity();
            }
        });
    }
    private void startOARRegionSelectorActivity() {
        Intent intent = new Intent(this, OARRegionSelectorActivity.class);
        startActivity(intent);
    }
    public void addListenerOnDoseLimitExplorerButtonClick() {
        doseLimitExplorerWelcomeButton = (Button) findViewById(R.id.doseexp_welcomebutton);
        doseLimitExplorerWelcomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDoseLimitExplorerActivity();
            }
        });

    }
    private void startDoseLimitExplorerActivity() {
        Intent intent = new Intent(this, DoseLimitExplorerRegionSelectorActivity.class);
        startActivity(intent);
    }
    public void addListenerOnTumorButtonClick() {
        tumorWelomeButton = (Button) findViewById(R.id.tumor_welcomebutton);
        tumorWelomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTumorDoseRecalculator();
            }
        });
    }
    private void startTumorDoseRecalculator() {
        Intent intent = new Intent(this, TumorDoseRecalculatorActivity.class);
        startActivity(intent);
    }
    public void addListenerOnHyperfxButtonClick() {
        hyperfxWelcomButton = (Button) findViewById(R.id.hyperfx_welcomebutton);
        hyperfxWelcomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startHyperfxCalculator();
            }
        });
    }
    private void startHyperfxCalculator() {
        Intent intent = new Intent(this, HyperfxCalculatorActivity.class);
        startActivity(intent);
    }
    public void addListenerOnRescheduleButtonClick() {
        rescheduleWelcomeButton = (Button) findViewById(R.id.reschedule_welcomebutton);
    }
    public void addListenerOnLQCalcButtonClick() {
        LQCalcWelcomeButton = (Button) findViewById(R.id.LQ_welcomebutton);
    }
    public void addListenerOnExtrasButtonClick() {
        extrasWelcomeButton = (Button) findViewById(R.id.extras_welcomebutton);
    }

    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("WelcomePage")
                .setUrl(Uri.parse("https://gitlab.com/prhone/RaDonk"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
