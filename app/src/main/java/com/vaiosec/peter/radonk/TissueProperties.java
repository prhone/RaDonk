package com.vaiosec.peter.radonk;

/**
 * Created by peter on 07/12/16.
 */

public class TissueProperties {
    int id;
    String _tissue_type;
    String _endpoint;
    double _alphabeta;
    //EQD2 is not a tissue property because it depends on fractionation, here for ListView recalcs
    double _eqd2;
    String _region;
    String _source;
    double _doselimit;

    public TissueProperties() {}
    public TissueProperties(String tissue_type, String endpoint, double alphabeta, String region, double doselimit) {
        _tissue_type=tissue_type;
        _endpoint=endpoint;
        _alphabeta=alphabeta;
        _region=region;
        _eqd2=0.0d;
        _doselimit=doselimit;
    }

    public int getID() {
        return this.id;
    }
    public String getTissueType() {
        return this._tissue_type;
    }
    public String getEndpoint() {
        return this._endpoint;
    }
    public String getSource() {
        return this._source;
    }
    public String getRegion() { return this._region; }
    public double getEQD2() { return this._eqd2; }
    public double getDoselimit() {return this._doselimit;}
    public double getAlphabeta() { return this._alphabeta; }
    public void setEQD2(double eqd2) { this._eqd2 = eqd2; }
}
