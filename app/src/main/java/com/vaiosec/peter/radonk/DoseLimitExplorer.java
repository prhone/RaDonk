package com.vaiosec.peter.radonk;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

/**
 * Created by peter on 18.12.16.
 */

public class DoseLimitExplorer extends AppCompatActivity {
        private GoogleApiClient client;

    private ListView doseLimitListview;
    private StrahlDatabaseAccess dbaccess;
    private ArrayAdapter<String> constraintsListAdapter;
    private ArrayList<String> constraintsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dose_limit_explorer);
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        doseLimitListview = (ListView) findViewById(R.id.doselimit_listview);

        setupDoseLimitListView();

    }

    private void setupDoseLimitListView() {
        dbaccess = StrahlDatabaseAccess.getInstance(this);
        dbaccess.open();
        ArrayList<String> dl_tmp = dbaccess.getDoseConstraints2Gy();
        dbaccess.close();
        constraintsList = new ArrayList<>();
        String region = getIntent().getStringExtra("region");
        for(String dl : dl_tmp) {
            if(dl.contains(";")) {
                dl=dl.replace("region: head;ent, ","");
                dl=dl.replace("region: ent;thorax, ","");
                dl=dl.replace("region: thorax;abdomen, ","");
                dl=dl.replace("organ: ","");
                dl=dl.substring(0,dl.length()-2);
                constraintsList.add(dl);
            }
            else if (dl.contains("region: "+region)) {
                dl=dl.replace("region: "+region+", ","");
                dl=dl.replace("organ: ","");
                dl=dl.substring(0,dl.length()-2);
                constraintsList.add(dl);
            }
        }
        constraintsListAdapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,constraintsList);
        doseLimitListview.setAdapter(constraintsListAdapter);
    }
}
