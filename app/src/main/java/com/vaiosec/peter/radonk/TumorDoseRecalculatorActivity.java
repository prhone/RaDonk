package com.vaiosec.peter.radonk;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

/**
 * Created by peter on 19.12.16.
 */

public class TumorDoseRecalculatorActivity extends AppCompatActivity {

    private Spinner fxValueSpinner;
    private EditText numFxEditText;
    private EditText totalDoseEditText;
    private ListView tumorListView;

    private ArrayAdapter<CharSequence> fxValueArrayAdapter;
    private TumorListAdapter tumorPropsAdapter;

    private StrahlDatabaseAccess dbaccess;
    private ArrayList<TissueProperties> tumorProps;
    private double fxDose;
    private int fxNum;
    private double totalDose;
    private static final String TAG = "Tumor_Activity";
    private String td_prev="50";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tumor_dose_recalculator);

        tumorListView = (ListView) findViewById(R.id.tumorListView);
        fxValueSpinner = (Spinner) findViewById(R.id.fxValueSpinner);
        totalDoseEditText = (EditText) findViewById(R.id.totalDoseEditText);
        numFxEditText = (EditText) findViewById(R.id.numFxEditText);

        addListenerOnFxDoseChange();
        addListenerOnNumFxChange();
        addListenerOnTotalDoseChange();
        setupTumorListView();
        setupFxSpinner();
        setupNumFxTextEdit();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void setupTumorListView() {
        dbaccess = StrahlDatabaseAccess.getInstance(this);
        dbaccess.open();

        //get all tissues
        tumorProps = dbaccess.getTumorProperties();
        dbaccess.close();
        tumorPropsAdapter = new TumorListAdapter(this, tumorProps);
        recalculateEQD2();
        tumorListView.setAdapter(tumorPropsAdapter);

    }

    private void setupFxSpinner() {
        fxValueArrayAdapter = ArrayAdapter.createFromResource(this, R.array.FxValueStrArray, R.layout.spinner_layout);
        fxValueSpinner.setAdapter(fxValueArrayAdapter);
        fxValueSpinner.setSelection(4); //default to 2.0 Gy
    }

    private void setupNumFxTextEdit() {
        numFxEditText.setText("25");
        setNumFx(25);
    }

    public void addListenerOnFxDoseChange() {
        fxValueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tmp_fxDose = parent.getItemAtPosition(position).toString();
                Log.v(TAG, tmp_fxDose);
                setFxDose(Double.parseDouble(tmp_fxDose));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
  public void addListenerOnTotalDoseChange() {
        totalDoseEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                String tmp_totaldose = totalDoseEditText.getText().toString();
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Log.v(TAG, tmp_totaldose);
                        Log.v(TAG, "action triggered (DONE)");
                        checkTotalDoseForConsistency(Double.parseDouble(tmp_totaldose));
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        Log.v(TAG, tmp_totaldose);
                        Log.v(TAG, "action triggered (NEXT)");
                        checkTotalDoseForConsistency(Double.parseDouble(tmp_totaldose));
                        break;
                    default:
                        Log.v(TAG, "no action triggered (default) IME: " + Integer.toString(result));
                        return true;
                }
                return false;
            }
        });

        totalDoseEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String td = totalDoseEditText.getText().toString();
                if(!hasFocus) {
                    if(td=="") td=td_prev;
                    checkTotalDoseForConsistency(Double.parseDouble(td));
                }
            }
        });

    }

    public void addListenerOnNumFxChange() {
        numFxEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                String tmp_fxNum = numFxEditText.getText().toString();
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Log.v(TAG, tmp_fxNum);
                        setNumFx(Integer.parseInt(tmp_fxNum));
                        Log.v(TAG, "action triggered (DONE)");
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        Log.v(TAG, tmp_fxNum);
                        setNumFx(Integer.parseInt(tmp_fxNum));
                        Log.v(TAG, "action triggered (NEXT)");
                        break;
                    default:
                        Log.v(TAG, "no action triggered (default) IME: " + Integer.toString(result));
                        return true;
                }
                return false;
            }
        });
        numFxEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String nfx = numFxEditText.getText().toString();
                if(!hasFocus){
                    setNumFx(Integer.parseInt(nfx));
                }
            }
        });
    }

    private void checkTotalDoseForConsistency(double tmp_totaldose) {
        double tmp_fxNum = tmp_totaldose / fxDose;
        setNumFx((int) Math.rint(tmp_fxNum));
    }

    public void setFxDose(double GyPerFraction) {
        fxDose = GyPerFraction;
        setTotalDose(fxDose * (double) fxNum);
    }

    public void setNumFx(int numberOfFractions) {
        fxNum = numberOfFractions;
        numFxEditText.setText(Integer.toString(fxNum));
        setTotalDose(fxDose * (double) fxNum);
    }

    public void setTotalDose(double totalDoseInGy) {
        totalDose = totalDoseInGy;
        totalDoseEditText.setText(String.format("%.2f", totalDose));
        recalculateEQD2();
    }

    public void recalculateEQD2() {
        for (TissueProperties tp : tumorProps) {
            tp.setEQD2(LQMathClass.EQD2(fxDose, tp.getAlphabeta(), totalDose));
        }
        tumorPropsAdapter.notifyDataSetChanged();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("TumorDoseCalcPage")
                .setUrl(Uri.parse("https://gitlab.com/prhone/RaDonk"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
