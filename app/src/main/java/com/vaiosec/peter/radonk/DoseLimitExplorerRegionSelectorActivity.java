package com.vaiosec.peter.radonk;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by peter on 17.12.16.
 */

public class DoseLimitExplorerRegionSelectorActivity extends AppCompatActivity {
    private GoogleApiClient client;

    private ImageButton head_explorerImageButton;
    private ImageButton ent_explorerImageButton;
    private ImageButton thorax_explorerImageButton;
    private ImageButton abdomen_explorerImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dose_limit_explorer_region_selection);
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        addListenerHeadSelected();
        addListenerENTSelected();
        addListenerThoraxSelected();
        addListenerAbdomenSelected();
    }

    public void addListenerHeadSelected() {
        head_explorerImageButton = (ImageButton) findViewById(R.id.head_explorerImageButton);
        head_explorerImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExplorerActivity("head");
            }
        });
    }
    public void addListenerENTSelected() {
        ent_explorerImageButton = (ImageButton) findViewById(R.id.ent_explorerImageButton);
        ent_explorerImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExplorerActivity("ent");
            }
        });
    }

    public void addListenerThoraxSelected() {
        thorax_explorerImageButton = (ImageButton) findViewById(R.id.thorax_explorerImageButton);
        thorax_explorerImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExplorerActivity("thorax");
            }
        });
    }

    public void addListenerAbdomenSelected() {
        abdomen_explorerImageButton = (ImageButton) findViewById(R.id.abdomen_explorerImageButton);
        abdomen_explorerImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExplorerActivity("abdomen");
            }
        });

    }

    public void startExplorerActivity(String region) {
        Intent intent = new Intent(this,DoseLimitExplorer.class);
        intent.putExtra("region",region);
        startActivity(intent);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("RaDonk Welcome")
                .setUrl(Uri.parse("https://gitlab.com/prhone/RaDonk"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
