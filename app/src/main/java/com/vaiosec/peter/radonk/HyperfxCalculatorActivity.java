package com.vaiosec.peter.radonk;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

/**
 * Created by peter on 19.12.16.
 */

public class HyperfxCalculatorActivity extends AppCompatActivity {

    private Spinner fxValueSpinner;
    private Spinner numDailyFxValueSpinner;
    private Spinner timeIntervalValueSpinner;
    private EditText numFxEditText;
    private EditText totalDoseEditText;
    private EditText alphabetaEditText;
    private AutoCompleteTextView recoveryHalftimeAutoCompleteTextView;
    private TextView EQD2TextView;

    private ArrayAdapter<CharSequence> fxValueArrayAdapter;
    private ArrayAdapter<CharSequence> numFxPerDayArrayAdapter;
    private ArrayAdapter<CharSequence> timeIntervalArrayAdapter;
    private ArrayAdapter<CharSequence> recoveryHalftimesArrayAdapter;
//    private TumorListAdapter tumorPropsAdapter;

    private StrahlDatabaseAccess dbaccess;
    private ArrayList<TissueProperties> tumorProps;
    private double _fxDose;
    private int _fxNum;
    private int _fxNumPerDay;
    private int _timeBetweenFx;
    private double _recoveryHalftime;
    private double _totalDose;
    private double _alphabeta;
    private HmTableClass _HmTables;
    private static final String TAG = "HyperFxCalc";
    private String td_prev="50";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hyperfx_calculator);

        fxValueSpinner = (Spinner) findViewById(R.id.fxValueSpinner);
        numDailyFxValueSpinner = (Spinner) findViewById(R.id.numDailyFxValueSpinner);
        timeIntervalValueSpinner = (Spinner) findViewById(R.id.timeIntervalValueSpinner);
        numFxEditText = (EditText) findViewById(R.id.numFxEditText);
        totalDoseEditText = (EditText) findViewById(R.id.totalDoseEditText);
        alphabetaEditText = (EditText) findViewById(R.id.alphabetaEditText);
        recoveryHalftimeAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.recoveryHalftimeAutoCompleteTextView);
        EQD2TextView = (TextView) findViewById(R.id.EQD2TextView);

        setupNumDailyFxSpinner();
        setupTimeIntervalSpinner();
        setupHmTables();
        setupFxSpinner();
        setupRecoveryHalftimeAutoComplete();
        setupNumFxTextEdit();

        addListenerOnFxSpinnerChange();
        addListenerOnNumDailyFxSpinnerChange();
        addListenerOnTimeIntervalSpinnerChange();
        addListenerOnHalftimeTextChange();
        addListenerOnNumFxChange();
        addListenerOnTotalDoseChange();
        addListenerOnAlphabetaChange();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void setupFxSpinner() {
        fxValueArrayAdapter = ArrayAdapter.createFromResource(this, R.array.FxValueStrArray, R.layout.spinner_layout);
        fxValueSpinner.setAdapter(fxValueArrayAdapter);
        fxValueSpinner.setSelection(4); //default to 2.0 Gy
        _fxDose=Double.parseDouble(fxValueSpinner.getSelectedItem().toString());
    }
    private void setupNumDailyFxSpinner() {
        numFxPerDayArrayAdapter = ArrayAdapter.createFromResource(this, R.array.FxPerDayStrArray, R.layout.spinner_layout);
        numDailyFxValueSpinner.setAdapter(numFxPerDayArrayAdapter);
        numDailyFxValueSpinner.setSelection(0); //default to 2 fx / day
        _fxNumPerDay=Integer.parseInt(numDailyFxValueSpinner.getSelectedItem().toString());
    }
    private void setupTimeIntervalSpinner() {
        timeIntervalArrayAdapter = ArrayAdapter.createFromResource(this, R.array.timeIntervalStrArray, R.layout.spinner_layout);
        timeIntervalValueSpinner.setAdapter(timeIntervalArrayAdapter);
        timeIntervalValueSpinner.setSelection(3); //default to 6 hrs between fractions
        _timeBetweenFx=Integer.parseInt((timeIntervalValueSpinner.getSelectedItem().toString()));
    }
    private void setupRecoveryHalftimeAutoComplete() {
        recoveryHalftimesArrayAdapter = ArrayAdapter.createFromResource(this, R.array.recoveryHalftimesStrArray, android.R.layout.simple_dropdown_item_1line);
        recoveryHalftimeAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.recoveryHalftimeAutoCompleteTextView);
        recoveryHalftimeAutoCompleteTextView.setAdapter(recoveryHalftimesArrayAdapter);
    }
    private void setupNumFxTextEdit() {
        numFxEditText.setText("25");
        setNumFx(25);
    }

    public void addListenerOnFxSpinnerChange() {
        fxValueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tmp_fxDose = parent.getItemAtPosition(position).toString();
                Log.v(TAG, tmp_fxDose);
                setFxDose(Double.parseDouble(tmp_fxDose));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void addListenerOnNumDailyFxSpinnerChange() {
       numDailyFxValueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               String tmp_numFxPerDay = parent.getItemAtPosition(position).toString();
               Log.v(TAG, tmp_numFxPerDay);
               setNumFxPerDay(Integer.parseInt(tmp_numFxPerDay));
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
    }
    public void addListenerOnTimeIntervalSpinnerChange() {
        timeIntervalValueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               String tmp_timeInt = parent.getItemAtPosition(position).toString();
                Log.v(TAG, tmp_timeInt);
                setTimeIntervalBetweenFx(Integer.parseInt(tmp_timeInt));
                recalculateEQD2();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addListenerOnHalftimeTextChange() {

        recoveryHalftimeAutoCompleteTextView.setOnEditorActionListener(new AutoCompleteTextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                String tmp_halftime = recoveryHalftimeAutoCompleteTextView.getText().toString();
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Log.v(TAG, tmp_halftime);
                        Log.v(TAG, "recoveryHalftimeAC action triggered (DONE)");
                        setRecoveryHalftime(Double.parseDouble(tmp_halftime));
                        recalculateEQD2();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        Log.v(TAG, tmp_halftime);
                        Log.v(TAG, "recoveryHalftimeAC action triggered (NEXT)");
                        setRecoveryHalftime(Double.parseDouble(tmp_halftime));
                        recalculateEQD2();
                        break;
                    default:
                        Log.v(TAG, "no action triggered (default) IME: " + Integer.toString(result));
                        return true;
                }
                return false;
            }
        });

        recoveryHalftimeAutoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    Log.v(TAG, "recoveryHalftimeAC gained focus");
                    recoveryHalftimeAutoCompleteTextView.showDropDown();
                }
                else {
                    Log.v(TAG, "recoveryHalftimeAC lost focus");
                    setRecoveryHalftime(Double.parseDouble(recoveryHalftimeAutoCompleteTextView.getText().toString()));
                    recalculateEQD2();
                }
            }
        });

        recoveryHalftimeAutoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "recoveryHalftimeAC clicked");
                recoveryHalftimeAutoCompleteTextView.showDropDown();
            }
        });

        recoveryHalftimeAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(TAG, "recoveryHalftimeAC item selected");
                setRecoveryHalftime(Double.parseDouble(recoveryHalftimesArrayAdapter.getItem(position).toString()));
                recalculateEQD2();
            }
        });

    }

    private void setupHmTables() {
        dbaccess = StrahlDatabaseAccess.getInstance(this);
        dbaccess.open();

        _HmTables = dbaccess.getHmTable(_fxNumPerDay);
        dbaccess.close();
        recalculateEQD2();
    }

    public void addListenerOnNumFxChange() {
        numFxEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                String tmp_fxNum = numFxEditText.getText().toString();
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Log.v(TAG, "NumFx action triggered (DONE)");
                        setNumFx(Integer.parseInt(tmp_fxNum));
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        Log.v(TAG, "NumFx action triggered (NEXT)");
                        setNumFx(Integer.parseInt(tmp_fxNum));
                        break;
                    default:
                        Log.v(TAG, "NumFx no action triggered (default) IME: " + Integer.toString(result));
                        return true;
                }
                return false;
            }
        });
        numFxEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String nfx = numFxEditText.getText().toString();
                if(!hasFocus){
                    Log.v(TAG, "NumFx lost focus");
                    setNumFx(Integer.parseInt(nfx));
                }
            }
        });
    }

    public void addListenerOnAlphabetaChange() {
        alphabetaEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                Double tmp_alphabeta = Double.parseDouble(alphabetaEditText.getText().toString());
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Log.v(TAG, "alphabeta action triggered (DONE)");
                        setAlphabeta(tmp_alphabeta);
                        recalculateEQD2();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        Log.v(TAG, "alphabeta action triggered (NEXT)");
                        setAlphabeta(tmp_alphabeta);
                        recalculateEQD2();
                        break;
                    default:
                        Log.v(TAG, "no action triggered (default) IME: " + Integer.toString(result));
                        return true;
                }
                return false;
            }
        });
        alphabetaEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!alphabetaEditText.getText().toString().isEmpty()) {
                    Double nab = Double.parseDouble(alphabetaEditText.getText().toString());
                    if(!hasFocus){
                        Log.v(TAG, "alphabeta lost focus");
                        setAlphabeta(nab);
                        recalculateEQD2();
                    }
                }
            }
        });
    }
    public void addListenerOnTotalDoseChange() {
        totalDoseEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                String tmp_totaldose = totalDoseEditText.getText().toString();
                switch (result) {
                    case EditorInfo.IME_ACTION_DONE:
                        Log.v(TAG, tmp_totaldose);
                        Log.v(TAG, "totaldose action triggered (DONE)");
                        checkTotalDoseForConsistency(Double.parseDouble(tmp_totaldose));
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        Log.v(TAG, tmp_totaldose);
                        Log.v(TAG, "totaldose action triggered (NEXT)");
                        checkTotalDoseForConsistency(Double.parseDouble(tmp_totaldose));
                        break;
                    default:
                        Log.v(TAG, "no action triggered (default) IME: " + Integer.toString(result));
                        return true;
                }
                return false;
            }
        });

        totalDoseEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                String td = totalDoseEditText.getText().toString();
                if(!hasFocus) {
                    if(td=="") td=td_prev;
                    Log.v(TAG, "totaldose lost focus");
                    checkTotalDoseForConsistency(Double.parseDouble(td));
                }
            }
        });

    }
    private void checkTotalDoseForConsistency(double tmp_totaldose) {
        double tmp_fxNum = tmp_totaldose / _fxDose;
        setNumFx((int) Math.rint(tmp_fxNum));
    }

    public void setFxDose(double GyPerFraction) {
        _fxDose = GyPerFraction;
        setTotalDose(_fxDose * (double) _fxNum);
    }

    public void setNumFx(int numberOfFractions) {
        _fxNum = numberOfFractions;
        numFxEditText.setText(Integer.toString(_fxNum));
        setTotalDose(_fxDose * (double) _fxNum);
    }

    public void setNumFxPerDay(int numFxPerDay) {
        _fxNumPerDay = numFxPerDay;
        setupHmTables();
    }

    public void setAlphabeta(double alphabeta) {
        _alphabeta = alphabeta;
    }
    public void setRecoveryHalftime(double recoveryHalftime) {
        _recoveryHalftime=recoveryHalftime;
    }
    public void setTimeIntervalBetweenFx(int timeIntervalBetweenFx) {
        _timeBetweenFx = timeIntervalBetweenFx;
    }
    public void setTotalDose(double totalDoseInGy) {
        _totalDose = totalDoseInGy;
        totalDoseEditText.setText(String.format("%.2f", _totalDose));
        recalculateEQD2();
    }
    public void recalculateEQD2() {
        //if halftime not exactly represented by table, find nearest above and below, and interpolate Hm value
        //otherwise get Hm value corresponding to halftime exactly
        //use correct table for 2 or 3 fractions / day
        if(_timeBetweenFx>0&&_fxNumPerDay>0&&_recoveryHalftime>0.0d) {
            Double[] halftimeHmPair = {0.0d,0.0d};
            halftimeHmPair = _HmTables.isExactMatch(_recoveryHalftime, _timeBetweenFx);
            Double[] lowerLimit = {0.0d,0.0d};
            Double[] upperLimit = {0.0d,0.0d};
            Double hm2_interpolated, eqd2;
            if (halftimeHmPair[0] < 0.0d) {
                lowerLimit = _HmTables.getLowerRepairHalftimeAndHm(_recoveryHalftime, _timeBetweenFx, _fxNumPerDay);
                upperLimit = _HmTables.getUpperRepairHalftimeAndHm(_recoveryHalftime, _timeBetweenFx, _fxNumPerDay);
                hm2_interpolated = LQMathClass.interpolateHm(upperLimit[1], lowerLimit[1], upperLimit[0], lowerLimit[0], _recoveryHalftime);
                eqd2 = LQMathClass.EQD2_Hm(_fxDose, _totalDose, _alphabeta, hm2_interpolated);
            } else {
                eqd2 = LQMathClass.EQD2_Hm(_fxDose, _totalDose, _alphabeta, halftimeHmPair[1]);
            }

            EQD2TextView.setText(String.format("%.2f", eqd2));
        }

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("HyperFxCalc_Page")
                .setUrl(Uri.parse("https://gitlab.com/prhone/RaDonk"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
