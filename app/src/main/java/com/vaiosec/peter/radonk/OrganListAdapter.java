package com.vaiosec.peter.radonk;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by peter on 07/12/16.
 */

public class OrganListAdapter extends ArrayAdapter<TissueProperties> {

    public OrganListAdapter(Context context, ArrayList<TissueProperties> a_tp) {
        super(context, 0, a_tp);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {

        TissueProperties tp = getItem(pos);
        DecimalFormat df = new DecimalFormat("##.##");

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_organ, parent, false);
        }

        TextView tissue = (TextView) convertView.findViewById(R.id.tissue_textView);
        tissue.setText(tp.getTissueType());

        TextView endpoint = (TextView) convertView.findViewById(R.id.endpoint_textView);
        endpoint.setText(tp.getEndpoint());

        TextView alphabeta = (TextView) convertView.findViewById(R.id.alphabeta_textView);
        alphabeta.setText(Double.toString(tp.getAlphabeta()));

        TextView eqd2 = (TextView) convertView.findViewById(R.id.eqd2_textView);
        if(tp.getEQD2()<tp.getDoselimit()) {
            eqd2.setBackgroundColor(Color.parseColor("#006600"));
        }
        else if (tp.getEQD2()>=tp.getDoselimit()) {
            eqd2.setBackgroundColor(Color.parseColor("#660000"));
        }
        eqd2.setText(df.format(tp.getEQD2()));


        return convertView;
    }
}
