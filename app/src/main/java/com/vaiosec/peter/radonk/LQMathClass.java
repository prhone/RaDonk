/**
 * Created by peter on 14/12/16.
 */

package com.vaiosec.peter.radonk;

public final class LQMathClass {
    private LQMathClass(){
    }
    public static double EQD2(double fractionalDose, double alphabeta, double totaldose){
        return totaldose*(fractionalDose+alphabeta)/(2.0d+alphabeta);
    }
    public static double EQD2_Hm(double fractionalDose, double totaldose, double alphabeta, double Hm) {
        return totaldose*(fractionalDose*(1.0d+Hm)+alphabeta)/(2.0d+alphabeta);
    }
    public static double interpolateHm(double Hm2, double Hm1, double halftime2, double halftime1, double tissueHalftime) {
        return tissueHalftime*(Hm2-Hm1)/(halftime2-halftime1);
    }
}
