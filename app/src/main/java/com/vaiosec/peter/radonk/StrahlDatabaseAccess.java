/**
 * Created by peter on 07/12/16.
 */

package com.vaiosec.peter.radonk;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class StrahlDatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static StrahlDatabaseAccess instance;
    private static HashMap<Integer,String> tableHeadings;

    private StrahlDatabaseAccess(Context context) {
        this.openHelper = new StrahlDatabaseHelper(context);
    }

    public static StrahlDatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new StrahlDatabaseAccess(context);
        }

        tableHeadings=new HashMap<>(23);
        tableHeadings.put(0,"region");
        tableHeadings.put(1,"organ");
        tableHeadings.put(2,"max");
        tableHeadings.put(3,"mean");
        tableHeadings.put(4,"median");
        tableHeadings.put(5,"V2");
        tableHeadings.put(6,"V10");
        tableHeadings.put(7,"V20");
        tableHeadings.put(8,"V25");
        tableHeadings.put(9,"V27");
        tableHeadings.put(10,"V30");
        tableHeadings.put(11,"V33");
        tableHeadings.put(12,"V40");
        tableHeadings.put(13,"V45");
        tableHeadings.put(14,"V47");
        tableHeadings.put(15,"V50");
        tableHeadings.put(16,"V60");
        tableHeadings.put(17,"V65");
        tableHeadings.put(18,"V70");
        tableHeadings.put(19,"V75");
        tableHeadings.put(20,"D1ccm");
        tableHeadings.put(21,"D20ccm");
        return instance;
    }
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    public void close() {
        if (database != null) {
            this.database.close();
        }
    }
    public List<String> getTissues() {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT tissue FROM alphabeta_human_tissues", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public ArrayList<TissueProperties> getTissueProperties() {
        ArrayList<TissueProperties> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT tissue,endpoint,alphabeta,region,doselimit FROM alphabeta_human_tissues ORDER BY tissue", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(new TissueProperties(cursor.getString(0),cursor.getString(1),cursor.getDouble(2),cursor.getString(3),cursor.getDouble(4)));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public ArrayList<String> getDoseConstraints2Gy() {
        ArrayList<String> list = new ArrayList<>();
        //read in first 22 columns
        int numCols=22;
        String tmpStr=new String();
        Cursor cursor = database.rawQuery("SELECT region,organ,max,mean,median,V2,V10,V20,V25,V27,V30,V33,V40,V45,V47,V50,V60,V65,V70,V75,D1ccm,D20ccm FROM dose_constraints_2gy ORDER BY organ", null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            for(int i=0; i<numCols; i++) {
                String curStr=cursor.getString(i);
                if(curStr!=null) {
                    tmpStr+=tableHeadings.get(i).toString()+": "+cursor.getString(i)+", ";
                }
            }
            list.add(tmpStr);
            tmpStr="";
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public ArrayList<TissueProperties> getTumorProperties() {
        ArrayList<TissueProperties> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT tumor,alphabeta FROM alphabeta_tumors ORDER BY tumor",null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            list.add(new TissueProperties(cursor.getString(0),null,cursor.getDouble(1),null,0.0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public HmTableClass getHmTable(int fxPerDay) {
        HmTableClass HmTable = new HmTableClass(fxPerDay);
        Cursor cursor;
        int i=0;
        switch(fxPerDay) {
            case 2:
                cursor = database.rawQuery("select dailyfx,repair_halftime,\"3hrs\",\"4hrs\",\"5hrs\",\"6hrs\",\"8hrs\",\"10hrs\" from HmFactors2fx",null);
                cursor.moveToFirst();
                while(!cursor.isAfterLast()) {
                    for(int j=0;j<cursor.getColumnCount();j++) {
                        HmTable.addTableEntry(i, j, cursor.getDouble(j));
                    }
                    cursor.moveToNext();
                    i++;
                }
                break;
            case 3:
                cursor = database.rawQuery("select dailyfx,repair_halftime,\"3hrs\",\"4hrs\",\"5hrs\",\"6hrs\",\"8hrs\" from HmFactors3fx",null);
                cursor.moveToFirst();
                while(!cursor.isAfterLast()) {
                    for(int j=0;j<cursor.getColumnCount();j++) {
                        HmTable.addTableEntry(i, j, cursor.getDouble(j));
                    }
                    cursor.moveToNext();
                    i++;
                }
                break;
        }
        return HmTable;
    }
}
